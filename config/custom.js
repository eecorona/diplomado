/**
 * Custom configuration
 * (sails.config.custom)
 *
 * One-off settings specific to your application.
 *
 * For more information on custom configuration, visit:
 * https://sailsjs.com/config/custom
 */

module.exports.custom = {
  cuentasFromName:'Erik Corona',
  cuentasFromEmail:'erik.corona.vasquez@gmail.com',
  registroEmailSubject: '✔ ¡Bienvenido! - Activación de tu cuenta.',
  registroActivadoEmailSubject: '✔ ¡Tu cuenta ha sido activada!.',

  defaultAdminPassword:'12345678',

  //configurar con su cuenta y direccion ip pero en el archivo local.js
  baseUrl: 'https://dominio.com', //sin "/" al final
  emailHostUser: '',
  emailHostPassword: ''
};

/*
 * Estructura del archivo config/local.js (si no existe, crearlo)
 *
 * Estas configuraciones tienen precedencia sobre las demas.
 *
 * Este archivo no se va en GIT!
 *
 * Cada seccion a sobreescribir debe ser referida.
 *

module.exports = {
  custom:{
    defaultAdminPassword: 'mipassword',
    baseUrl: 'http://192.168.1.78:1337',
    emailHostUser:'usuario@gmail.com',
    emailHostPassword:'clavesecretadegmail'
  }
};

*/
