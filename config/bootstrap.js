/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs just before your Sails app gets lifted.
 * > Need more flexibility?  You can also do this by creating a hook.
 *
 * For more information on bootstrapping your app, check out:
 * https://sailsjs.com/config/bootstrap
 */

module.exports.bootstrap = async function(done) {

  //verificar si tenemos localidades en la base de datos, si no importarlas desde txt
  if(await Localidades.count()===0){
    sails.log('·• Inicializando Catálogo de Códigos Postales... (data/cp_oaxaca.txt)');
    sails.log('·  Esta tarea es asyncrona, puede demorar unos minutos, continuando sails lift.');

    //inicializamos un lector de lineas
    var lineReader = require('readline').createInterface({
      input: require('fs').createReadStream('data/cp_oaxaca.txt') //con el contenido del archivo de texto...
    });

    var codigos = [];
    //por cada linea:
    lineReader.on('line', (line) => {
      var partes = line.split('|');
      codigos.push({
        codigo: partes[0],
        asenta: partes[1],
        tipoAsenta: partes[2],
        mnpio: partes[3],
        estado: partes[4],
        ciudad: partes[5]
      });
    });

    lineReader.on('close', () => {
      Localidades.createEach(codigos).exec((err) => {
        if(err) {sails.log('Error guardando: ' + partes[0], partes[1]);}
        sails.log(' ✓ La inicialización del Catalogo de Códigos Postales  ha finalizado.');
      });
    });
  }

  //si no hay usuarios crear el inicial
  if(await Usuarios.count()===0){
    await Usuarios.create({
      nombre: 'Administrador',
      usuario: 'admin',
      password: await sails.helpers.passwords.hashPassword(sails.config.custom.defaultAdminPassword),
      email: 'admin@dominio.com',
      perfil: 'admin'
    });
    sails.log(' ✓ Cuenta de Administrador creada: admin');
    return done();
  }else{

    // Don't forget to trigger `done()` when this bootstrap function's logic is finished.
    // (otherwise your server will never lift, since it's waiting on the bootstrap)
    return done();
  }

};
