/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

module.exports.policies = {

  /***************************************************************************
  *                                                                          *
  * Default policy for all controllers and actions, unless overridden.       *
  * (`true` allows public access)                                            *
  *                                                                          *
  ***************************************************************************/

  '*': ['sesion'], //TODO requiere por lo menos sesion
  'usuarios/*': ['sesion','esadmin'],
  'usuarios/yo':['sesion'],
  //rutas libres
  'alumnos/registro':true,
  'alumnos/activar': true,
  'auth/login': true

};
