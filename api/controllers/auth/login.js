module.exports = {
  friendlyName: 'Login de Usuario',
  description: 'Recibe usuario y contraseña y crea la sesion del usuario.',
  inputs: {
    usuario:{
      type:'string',
      required: true
    },
    password:{
      type:'string',
      required: true
    }
  },
  exits: {
    success:{
      statusCode: 200,
      description:'El usuario inició sesion correctamente, se retorna el usuario sin password.'
    },
    error:{
      statusCode: 500,
      description: 'Ocurrió un error de sistema.'
    },
    badCombo:{
      statusCode: 403,
      description:'La contraseña introducida no es valida.'
    }
  },
  fn: async function (inputs, exits) {

    var model = 'usuarios';
    var query = {usuario: inputs.usuario};

    //diferenciar usuarios y alumnos
    if(inputs.usuario.indexOf('@')>-1) {
      model='alumnos';
      query = {email: inputs.usuario};
    }

    //Buscar al usuario/alumno sails.models.alumnos
    var usr = await sails.models[model].findOne(query);

    //el usuario no existe
    if(!usr) {return exits.badCombo('Credenciales no válidas.');}

    //validar password
    await sails.helpers.passwords.checkPassword(inputs.password, usr.password).intercept('incorrect', 'badCombo','Credenciales no válidas');

    //avisar en sockets este inicio
    sails.sockets.broadcast('principal','usrLogin', usr);

    //iniciar sesion;
    this.req.session.usuario = usr;

    return exits.success(usr); //OK!
  }
};
