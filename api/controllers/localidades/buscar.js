module.exports = {


  friendlyName: 'Buscar',


  description: 'Buscar localidades.',


  inputs: {
    term:{
      type:'string',
      required:true
    }
  },


  exits: {

  },


  fn: async function (inputs, exits) {
    var lo = await Localidades.find({codigo:inputs.term});
    return exits.success(lo);
  }

};
