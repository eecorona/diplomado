module.exports = {
  friendlyName: 'Yo',
  description: 'Yo usuarios.',
  inputs: {

  },
  exits: {

  },
  fn: async function (inputs, exits) {
    if(this.req.isSocket){
      sails.log('Un socket ha llegado!', this.req.socket.id);
      var canales = ['principal'];

      //unirlo al canal principal (TODO MUNDO)
      sails.sockets.join(this.req, 'principal');

      //si trae perfil, unirlo a su canal de perfil
      if(this.req.session.usuario.perfil){
        sails.sockets.join(this.req, this.req.session.usuario.perfil);
        canales.push(this.req.session.usuario.perfil);
      }

      return exits.success({canales:canales});
    }else{
      return exits.success(this.req.me);
    }
  }
};
