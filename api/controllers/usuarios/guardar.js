module.exports = {
  friendlyName: 'Guardar',
  description: 'Guardar usuarios.',
  inputs: {
    usuario:{
      type:'json',
      required:true
    }
  },

  exits: {

  },
  fn: async function (inputs, exits) {
    var usr;

    if(inputs.usuario.password){
      inputs.usuario.password = await sails.helpers.passwords.hashPassword(inputs.usuario.password);
    }

    if(inputs.usuario.id){
      //modificar
      usr = await Usuarios.update({id:inputs.usuario.id}, inputs.usuario).fetch();
      sails.sockets.broadcast('admin','usuarioActualizado', usr);
      return exits.success(usr);
    }else{
      //crear
      usr = await Usuarios.create(inputs.usuario).fetch();
      sails.sockets.broadcast('admin','usuarioAgregado', usr);
      return exits.success(usr);
    }

  }
};
