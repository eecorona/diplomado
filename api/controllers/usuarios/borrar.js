module.exports = {
  friendlyName: 'Borrar',
  description: 'Borrar usuarios.',
  inputs: {
    id: {
      type:'string',
      required: true
    }
  },
  exits: {

  },
  fn: async function (inputs, exits) {
    var borrados = await Usuarios.destroy({id:inputs.id}).fetch();
    if(borrados && borrados.length){ //avisar a todos los usuarios del canal admin
      sails.sockets.broadcast('admin','usuarioBorrado', borrados);
    }
    return exits.success();
  }
};
