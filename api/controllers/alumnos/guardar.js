module.exports = {
  friendlyName: 'Guardar',
  description: 'Guardar alumnos.',
  inputs: {
    alumno:{
      type:'json',
      required:true
    }
  },

  exits: {

  },
  fn: async function (inputs, exits) {
    var usr;

    if(inputs.alumno.password){
      inputs.alumno.password = await sails.helpers.passwords.hashPassword(inputs.alumno.password);
    }

    if(inputs.alumno.id){
      //modificar
      usr = await Alumnos.update({id:inputs.alumno.id}, inputs.alumno).fetch();
      return exits.success(usr);
    }else{
      //crear
      //agregarle el usuario en sesion
      inputs.alumno.usuario = this.req.me.id;
      usr = await Alumnos.create(inputs.alumno).fetch();
      return exits.success(usr);
    }

  }
};
