module.exports = {
  friendlyName: 'Activar',
  description: 'Activar alumnos.',
  inputs: {
    token:{
      type:'string',
      required:true
    }
  },

  exits: {

  },
  fn: async function (inputs, exits) {
    var registro = await Alumnos.findOne({token:inputs.token});
    if(!registro) {return exits.success({result: false, err:'Registro no encontrado.'});}
    if(registro.activo) {return exits.success({result: false, err:'Su registro ya fué activado con anterioridad.'});}
    var alumno = await Alumnos.update({token:inputs.token},{plainPassword: password, password:passwordCryp, activo:true}).fetch();
    sails.log('Alumno activado:', alumno);
    //enviar email.
    var htmlEmail = await sails.renderView('email/activacion', {alumno:alumno[0], url:sails.config.custom.baseUrl});
    notificaService.registroActivado(alumno[0], htmlEmail);
    sails.sockets.broadcast('admin','alumnoActualizado', alumno);
    return exits.success({result:true, registro:alumno[0]});
  }
};
