module.exports = {
  friendlyName: 'Guardar imagen',
  description: '',
  inputs: {
    alumno:{
      type:'string',
      required: false,
      descripcion: 'id del alumno'
    }
  },
  exits: {
  },
  fn: async function (inputs, exits) {
    var self = this;
    var rutaFinal;

    Alumnos.findOne({id: inputs.alumno}).exec((err, alumno) => {
      if(err) {return exits.error(err);}

      if(!alumno) {return exits.error('No existe el alumno.');}

      self.req.file('imagen').upload({
        dirname:'../../assets/images/alumnos',
        saveAs: function(fileStream, next){
          sails.log('Archivo recibido: ',fileStream);
          if(fileStream.headers['content-type']==='image/jpeg') {
            rutaFinal = alumno.id+'.jpg';
            return next(null, rutaFinal);
          }else{
            return next('Archivo no válido, solo se permiten archivos JPG.');
          }
        }
      },(err, uploadedFiles) => {
        if (err) {
          sails.log.error('Error al subir imagen!', err);
          return exits.error(err);
        }
        sails.log('Imagen subida:', uploadedFiles);

        Alumnos.update({id:alumno.id},{foto:true}).meta({fetch:true}).exec((err, update) => {
          if(err) {return exits.error(err);}
          sails.sockets.broadcast('admin', 'alumnoActualizado', update);
          return exits.success(update);
        });
      });
    });
  }
};
