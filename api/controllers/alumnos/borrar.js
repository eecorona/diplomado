module.exports = {
  friendlyName: 'Borrar',
  description: 'Borrar alumnos.',
  inputs: {
    id: {
      type:'string',
      required: true
    }
  },
  exits: {

  },
  fn: async function (inputs, exits) {
    var borrados = await Alumnos.destroy({id:inputs.id}).fetch();
    if(borrados && borrados.length){ //avisar a todos los usuarios del canal admin
      sails.sockets.broadcast('admin','alumnoBorrado', borrados);
    }
    return exits.success(borrados);
  }
};
