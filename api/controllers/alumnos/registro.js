module.exports = {
  friendlyName: 'Registro',
  description: 'Registro alumnos.',
  inputs: {
    registro:{
      type:'json',
      required:true
    }
  },
  exits: {

  },
  fn: async function (inputs, exits) {

    //verficar que no exista!
    if(await Alumnos.count({email:inputs.registro.email})>=1){
      return exits.success({result:false, err:'Ya está registrado.'});
    }

    //generarle un token...
    var token = await sails.helpers.strings.uuid();
    inputs.registro.token = token;

    //crearlo
    var alumno = await Alumnos.create(inputs.registro).fetch();

    //emitir evento "alumnoAgregado" al canal "admin" con un payload alumno
    sails.sockets.broadcast('admin', 'alumnoAgregado', alumno);

    //parsear la plantilla de email
    var htmlEmail = await sails.renderView('email/registro', {alumno:alumno, url:sails.config.custom.baseUrl});

    //enviar email.
    notificaService.emailRegistro(alumno, htmlEmail);

    return exits.success({result:true, registro:alumno});
  }
};
