/*eslint-disable camelcase*/
/*
███████ ███    ███  █████  ██ ██
██      ████  ████ ██   ██ ██ ██
█████   ██ ████ ██ ███████ ██ ██
██      ██  ██  ██ ██   ██ ██ ██
███████ ██      ██ ██   ██ ██ ███████
*/

var nodemailer = require('nodemailer');

var cuentasMailer = nodemailer.createTransport({
  service:'gmail',
  auth: {
    user: sails.config.custom.emailHostUser,
    pass: sails.config.custom.emailHostPassword
  }
});
/*
███    ██ ██    ██ ███████ ██    ██  █████   ██████ ██    ██ ███████ ███    ██ ████████  █████
████   ██ ██    ██ ██      ██    ██ ██   ██ ██      ██    ██ ██      ████   ██    ██    ██   ██
██ ██  ██ ██    ██ █████   ██    ██ ███████ ██      ██    ██ █████   ██ ██  ██    ██    ███████
██  ██ ██ ██    ██ ██       ██  ██  ██   ██ ██      ██    ██ ██      ██  ██ ██    ██    ██   ██
██   ████  ██████  ███████   ████   ██   ██  ██████  ██████  ███████ ██   ████    ██    ██   ██
*/

module.exports.emailRegistro = function(alumno, htmlEmail, cb){

  var mailCuenta = {
    from: '"'+sails.config.custom.cuentasFromName+'" <'+ sails.config.custom.cuentasFromEmail+'>',
    to: alumno.email,
    subject: sails.config.custom.registroEmailSubject,
    html: htmlEmail
  };

  cuentasMailer.sendMail(mailCuenta, (error, resultado)=> {
    if (error) {
      sails.log.warn('No se pudo enviar email de registro:', error);
      if(typeof cb === 'function') {return cb(error, null);}
    }
    sails.log('Email de registro enviado:', resultado);
    if(typeof cb === 'function') {return cb(null, resultado);}
  });
};

module.exports.registroActivado = function(alumno, htmlEmail, cb){

  var mailCuenta = {
    from: '"'+sails.config.custom.cuentasFromName+'" <'+ sails.config.custom.cuentasFromEmail+'>',
    to: alumno.email,
    subject: sails.config.custom.registroActivadoEmailSubject,
    html: htmlEmail
  };

  cuentasMailer.sendMail(mailCuenta, (error, resultado)=> {
    if (error) {
      sails.log.warn('No se pudo enviar email de registro:', error);
      if(typeof cb === 'function') {return cb(error, null);}
    }
    sails.log('Email de registro enviado:', resultado);
    if(typeof cb === 'function') {return cb(null, resultado);}
  });
};
