var randomstring = require('randomstring');

/**
 * Alumnos.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    nombre:{
      type:'string',
      required: true
    },
    email:{
      type:'string',
      isEmail:true,
      required: true
    },
    token:{
      type:'string',
      required: false
    },
    activo:{
      type:'boolean',
      defaultsTo:false
    },
    plainPassword:{
      type:'string',
      required: false
    },
    password:{
      type:'string',
      required: false,
      protect:true
    },
    perfil:{
      type:'string',
      defaultsTo:'alumno',
      isIn:['alumno']
    },
    direccion:{
      type:'json',
      required:false
    },
    lastSeenAt:{
      type:'number',
      columnType:'date'
    },
    foto:{
      type:'boolean',
      defaultsTo:false
    },
    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝


    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝

    usuario:{
      model:'usuarios'
    }

  },
  customToJSON: function(){
    //quitar el password en cada que se regresa
    return _.omit(this,['password']);
  },
  beforeCreate: async function(registro, continuar){
    //procesar registro, generar password si no lo trae
    if(!registro.password){
      registro.password = randomstring.generate(8);
      sails.helpers.passwords.hashPassword(registro.password).exec((err, hash) => {
        if(err) {
          sails.log('Model Alumnos->beforeCreate Error:', err);
          return continuar(err); //la creacion se interrumpe y regresa el error
        }
        registro.passwordCryp = hash;
        return continuar(); //si lo regresamos vacio, continua la creacion
      });
    }
  },
  afterCreate: function(alumno, next){
    sails.sockets.broadcast('admin','alumnoAgregado', alumno);
    return next();
  },
  afterUpdate: function(alumno, next){
    sails.sockets.broadcast('admin','alumnoActualizado', alumno);
    return next();
  }
  //beforeUpdate
  //beforeDelete
  //after

};
