/*eslint-disable camelcase*/
var randomstring = require('randomstring');

/**
 * Usuarios.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {
    //  ╔═╗╦═╗╦╔╦╗╦╔╦╗╦╦  ╦╔═╗╔═╗
    //  ╠═╝╠╦╝║║║║║ ║ ║╚╗╔╝║╣ ╚═╗
    //  ╩  ╩╚═╩╩ ╩╩ ╩ ╩ ╚╝ ╚═╝╚═╝

    nombre:{
      type:'string',
      required: true
    },
    usuario:{
      type:'string',
      required: true
    },
    email:{
      type:'string',
      required: true
    },
    plainPassword:{
      type:'string',
      required: false
    },
    password:{
      type:'string',
      required: true,
      protect: true
    },
    perfil:{
      type:'string',
      required: false,
      defaultsTo:'admin',
      isIn:['admin','supervisor','caja','gerente']
    },
    lastSeenAt:{
      type:'number',
      columnType:'date'
    },

    //  ╔═╗╔╦╗╔╗ ╔═╗╔╦╗╔═╗
    //  ║╣ ║║║╠╩╗║╣  ║║╚═╗
    //  ╚═╝╩ ╩╚═╝╚═╝═╩╝╚═╝


    //  ╔═╗╔═╗╔═╗╔═╗╔═╗╦╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
    //  ╠═╣╚═╗╚═╗║ ║║  ║╠═╣ ║ ║║ ║║║║╚═╗
    //  ╩ ╩╚═╝╚═╝╚═╝╚═╝╩╩ ╩ ╩ ╩╚═╝╝╚╝╚═╝
    alumnos:{
      collection:'alumnos',
      via: 'usuario'
    }
  },
  customToJSON: function(){ //inyectando este codigo junto con JSON.stringify() (!async)
    //quitar el password en cada que se regresa un registro
    return _.omit(this,['password']);
  },
  beforeCreate: async function(registro, continuar){
    //procesar registro, generar password si no lo trae
    if(!registro.password){
      registro.password = randomstring.generate(8);
      sails.helpers.passwords.hashPassword(registro.password).exec((err, hash) => {
        if(err) {
          sails.log('Model Alumnos->beforeCreate Error:', err);
          return continuar(err); //la creacion se interrumpe y regresa el error
        }
        registro.passwordCryp = hash;
        return continuar(); //si lo regresamos vacio, continua la creacion
      });
    }
  },
};
