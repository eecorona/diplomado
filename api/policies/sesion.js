
module.exports = async function (req, res, proceed) {

  if (req.session.usuario && req.session.usuario.id) {
    return proceed();
  }

  return res.sendStatus(401);
};
