
module.exports = async function (req, res, proceed) {

  if (req.session.usuario && req.session.usuario.perfil==='admin') {
    return proceed();
  }

  return res.sendStatus(403);
};
