module.exports = function (sails) {

  // Variable para referencia a este hook.
  var hook;

  return {

    initialize: function(cb) {
      sails.log.info('. Initializing hook... (`api/hooks/contador-solicitudes`)');

      hook = this;

      hook.numSolicitudes = 0;
      hook.numSolicitudesAtendidas = 0;

      //inicializacion de hook completa
      return cb();
    },

    routes: {
      before: {
        '/*': function (req, res, next) {
          hook.numSolicitudes++;
          return next();
        }
      },
      after: {
        '/*': function (req, res, next) {
          hook.numSolicitudesAtendidas++;
          return next();
        }
      }
    }
  };
};

//variables disponibles en cualquier parte por medio de la global sails:
//sails.hooks["contador-solicitudes"].numSolicitudes y sails.hooks["contador-solicitudes"].numSolicitudesAtendidas
