/**
 * custom hook
 *
 * @description :: A hook definition.  Extends Sails by adding shadow routes, implicit actions, and/or initialization logic.
 * @docs        :: https://sailsjs.com/docs/concepts/extending-sails/hooks
 */

module.exports = function defineCustomHook(sails) {

  return {

    /**
     * Runs when a Sails app loads/lifts.
     *
     * @param {Function} done
     */
    initialize: async function (done) {
      sails.log.info('. Initializing hook... (`api/hooks/sesion`)');
      return done();
    },


    routes: {

      /**
       * Antes de cada ruta...
       *
       * @param {Ref} req
       * @param {Ref} res
       * @param {Function} next
       */
      before: {
        '/*': {
          skipAssets: true,
          fn: async function(req, res, next){


            // No existe sesion? proceder.
            if (!req.session) { return next(); }

            // No ha iniciado sesion? proceder.
            if (!req.session.usuario || !req.session.usuario.id) { return next(); }

            // Actualizar al usuario cuando fue la ultima vez que fue visto.
            //
            // (Nota: Por optimizacion esto corre async y no generar latencia adicional no necesaria.)
            //
            var MS_TO_BUFFER = 60*1000; //actualizar cada 60s max
            var now = Date.now();
            if (req.session.usuario.lastSeenAt < now - MS_TO_BUFFER) {
              Usuarios.update({id: req.session.usuario.id})
              .set({ lastSeenAt: now })
              .exec((err)=>{
                if (err) {
                  sails.log.error('Error en tarea de fondo: No se pudo actualizar al usuario (`'+req.session.usuario.id+'`) con la nueva fecha en que fue visto `lastSeenAt`.  Error: '+err.stack);
                  return;
                }
                req.session.usuario.lastSeenAt = now;
                sails.log.verbose('Actualizado `lastSeenAt` para el usuario `'+req.session.usuario.id+'`.');
              });
            }

            return next();
          }
        }
      }
    }


  };

};
