/*eslint-disable no-unused-vars*/
var ng = angular.module('diplomadoSnell', [
  'ngRoute', //dependencia inyectada
  'ngSails',
  'ngSanitize',
  'angularFileUpload'
]);

/**
 * La etapa run se ejecuta despues de la de config, puede haber varias en distintos
 * archivos de la aplicacion, como en routes.
 */
ng.run(['$rootScope', '$location', '$http','UsuariosFactory', 'AlumnosFactory',
  function($rootScope, $location, $http, UsuariosFactory, AlumnosFactory) {
  //objeto global para transporte de informacion compartida entre controllers.
    $rootScope.main = {
      app: {
        name: 'Diplomado SNEL',
        version: '0.3.0',
        author: '@kuuijes',
        authorLink: 'http://www.twitter.com/kuuijes'
      },
      conectado: false,
      rutaActual:'', //almacenamiento para la ruta actual del sistema.
      usuario: UsuariosFactory, //aqui guardamos el factory de usuario de forma global para poder tener acceso a el.
      alumnos: AlumnosFactory,
      //el header va a cambiar segun la sesion que se tenga, siendo por default header.html
      //login, init y logout en el factory de usuario lo controlan.
      header: UsuariosFactory.header,
      footer: 'js/app/templates/footer.html',
    };

    //inicializar el usuario
    UsuariosFactory.init();

  //fin de inicializacion de app
  }]);
