ng.run(['$rootScope', '$location', '$templateCache', '$http',
  function($rootScope, $location, $templateCache, $http) {

    /**
     * cargar las páginas en caché acelera su despliegue ya que son traidas todas al inicio y almacenadas.
     * el control de este caché sucede dentro de $http por si solo.
     */

    $http.get('js/app/templates/pages/loginController.html', { cache: $templateCache });
    $http.get('js/app/templates/pages/dashboardController.html', { cache: $templateCache });

    //Control de sesión.

    /**
     * EVENTO: $rootScope -> $routeChangeStart
     * Este evento se dispara ANTES de cambiar a una ruta, current puede contener null
     * cuando es la primera ruta que se va a cargar.
     *
     * @param  {type} event               evento
     * @param  {type} next                Contiene la ruta que se pretende cargar.
     * @param  {type} current             Contiene la ruta actual
     */
    $rootScope.$on('$routeChangeStart', (event, next) => {
      //console.log('Intento de cambio de ruta', next);

      var destino = (next.$$route && next.$$route.originalPath) ? next.$$route.originalPath : '';

      var rutasLibres = ['/login', '/acerca', '/registro', '/alumnos/:activar/:token?'];

      //si la ruta a la que desea entrar está entre las libres lo dejamos pasar.
      if (rutasLibres.indexOf(destino) > -1) {
        return true;
      }

      //De lo contrario, verificamos que tengamos un usuario en memoria.
      //si no lo tenemos, interrumpimos el evento, mostramos mensaje y lo mandamos al login.
      //Aqui tambien se podrian verificar permisos por página.
      else if(!$rootScope.main.usuario.yo.id){
        event.preventDefault(); //no entrar a la pagina
        //alertify.error('Debe iniciar sesión.');
        $location.path('/login').replace();
      }else{ //si tenemos usuario, local

        //checar el perfil si viene en la ruta
        if(next && next.$$route && next.$$route.acceso && next.$$route.acceso.perfil ){ //esta ruta requiere un perfil especifico
          if(next.$$route.acceso.perfil.indexOf($rootScope.main.usuario.yo.perfil) > -1){
            return true;
          }else{
            event.preventDefault();
            alertify.warning('No tiene permiso de ver.');
          }
        }else{
          return true;
        }
      }
    });


    /**
     * EVENTO: $rootScope -> $routeChangeSuccess
     *
     * Se dispara cuando una ruta ha finalizado su cambio.
     * La usaremos para almacenar la ruta actual en $rootScope.main.rutaActual
     *
     * prev puede contener null si es el primer cambio.
     */
    $rootScope.$on('$routeChangeSuccess', (event, current) => {
      if(current && current.$$route && current.$$route.originalPath) {
        $rootScope.main.rutaActual = current.$$route.originalPath;
        //console.log('ruta actual:', $rootScope.main.rutaActual);
      }
    });
  }]);



/**
 * Etapa Config
 *
 * Se ejecuta antes que run, aun no existe $rootScope y se tiene acceso a los
 * constructores de distintos servicios de angular para pre-configurarlos como en
 * este caso a proveedor de ubicaciones para quitar el ! de las rutas y al de rutas
 * para configurar las rutas de la SPA
 *
 */
ng.config(['$routeProvider', '$locationProvider', '$sailsProvider',function($routeProvider, $locationProvider, $sailsProvider) {


  $sailsProvider.config.transports = ['websocket']; //solo usar transport "websocket" (sails solo soporta este)
  $sailsProvider.config.reconnection = true; //reconectarme en caso de desconexion
  $sailsProvider.config.reconnectionDelay = 3000; //intentar reconectar cada N ms
  $sailsProvider.config.autoConnect = true; //conectar al inicio (default)

  /**
   * Por default AngularJS usa el bang (!) para las rutas junto con el hash (#)
   * aqui lo desactivamos.
   */

  $locationProvider.hashPrefix('');

  /**
   * Definición de rutas de la SPA
   */

  $routeProvider
    .when('/login', {
      templateUrl: 'js/app/templates/pages/loginController.html',
      controller: 'loginController'
    })
    .when('/dashboard', {
      templateUrl: 'js/app/templates/pages/dashboardController.html',
      controller: 'dashboardController',
    })
    .when('/acerca', {
      templateUrl: 'js/app/templates/pages/acercaController.html',
      controller: 'acercaController',
    })
    .when('/registro', {
      templateUrl: 'js/app/templates/pages/registroController.html',
      controller: 'registroController',
    })
    .when('/alumnos/:activar/:token?', {
      templateUrl: 'js/app/templates/pages/registroController.html',
      controller: 'registroController'
    })
    .when('/usuarios', {
      templateUrl: 'js/app/templates/pages/usuariosController.html',
      controller: 'usuariosController',
      acceso:{
        perfil:['admin']
      }
    })
    .when('/usuario/:id', {
      templateUrl: 'js/app/templates/pages/usuarioController.html',
      controller: 'usuarioController',
      acceso:{
        perfil:['admin']
      }
    })
    .when('/alumnos', {
      templateUrl: 'js/app/templates/pages/alumnosController.html',
      controller: 'alumnosController',
      acceso:{
        perfil:['admin','supervisor']
      }
    })
    .when('/alumno/:id', {
      templateUrl: 'js/app/templates/pages/alumnoController.html',
      controller: 'alumnoController',
      acceso:{
        perfil:['admin','supervisor']
      }
    })
    //por default, al cargar las rutas, esta será la primer vista que se va a cargar.
    .otherwise({
      redirectTo: '/dashboard'
    });
}]);
