ng.run(['$rootScope', '$sails', function($rootScope, $sails){


  $sails.on('connect', () => {
    console.log('Nos hemos conectado al socket!');
    $rootScope.main.conectado = true;
  });

  $sails.on('disconnect', () => {
    console.log('Nos hemos desconectado del socket!');
    $rootScope.main.conectado = false;
  });

  /*
  ██    ██ ███████ ██    ██  █████  ██████  ██  ██████  ███████
  ██    ██ ██      ██    ██ ██   ██ ██   ██ ██ ██    ██ ██
  ██    ██ ███████ ██    ██ ███████ ██████  ██ ██    ██ ███████
  ██    ██      ██ ██    ██ ██   ██ ██   ██ ██ ██    ██      ██
   ██████  ███████  ██████  ██   ██ ██   ██ ██  ██████  ███████
  */

  $sails.on('usuarioAgregado', (usuario) => {
    console.log('SOCKET->usuarioAgregado', usuario);
    $rootScope.main.usuario.usuarios.push(usuario);
  });

  $sails.on('usuarioActualizado', (usuario) => {
    console.log('SOCKET->usuarioAgregado', usuario);
    //buscarlo
    var idx = _.findIndex($rootScope.usuario.usuarios, {id:usuario.id});
    if(idx>-1){
      //modificarlo
      $rootScope.usuario.usuarios[idx] = usuario;
    }else{
      //si no existe, agregarlo
      $rootScope.main.usuario.usuarios.push(usuario);
    }
  });

  $sails.on('usuarioBorrado', (usuarios) => {
    console.log('SOCKET->usuarioBorrado', usuarios);
    usuarios.forEach((usuario) => {
      $rootScope.main.usuario.quitar(usuario);
      $rootScope.$broadcast('usuarioBorrado', usuario); //reenviar el evento desde la app hacia todos lados
    });
  });

  /*
   █████  ██      ██    ██ ███    ███ ███    ██  ██████  ███████
  ██   ██ ██      ██    ██ ████  ████ ████   ██ ██    ██ ██
  ███████ ██      ██    ██ ██ ████ ██ ██ ██  ██ ██    ██ ███████
  ██   ██ ██      ██    ██ ██  ██  ██ ██  ██ ██ ██    ██      ██
  ██   ██ ███████  ██████  ██      ██ ██   ████  ██████  ███████
  */

  $sails.on('alumnoAgregado', (alumno) => {
    console.log('SOCKET->alumnoAgregado', alumno);
    $rootScope.main.alumnos.data.push(alumno);
  });

  $sails.on('alumnoActualizado', (alumno) => {
    console.log('SOCKET->alumnoActualizado', alumno);
    //buscarlo
    var idx = _.findIndex($rootScope.main.alumnos.data, {id:alumno.id});
    if(idx>-1){
      //modificarlo
      $rootScope.main.alumnos.data[idx] = alumno;
    }else{
      //si no existe, agregarlo
      $rootScope.main.alumnos.data.push(alumno);
    }
  });

  $sails.on('alumnoBorrado', (alumnos) => {
    console.log('SOCKET->alumnoBorrado', alumnos);
    alumnos.forEach((alumno) => {
      $rootScope.main.alumnos.quitar(alumno);
      $rootScope.$broadcast('alumnoBorrado', alumno); //reenviar el evento desde la app hacia todos lados
    });
  });

  /*
  ██       ██████   ██████  ██ ███    ██
  ██      ██    ██ ██       ██ ████   ██
  ██      ██    ██ ██   ███ ██ ██ ██  ██
  ██      ██    ██ ██    ██ ██ ██  ██ ██
  ███████  ██████   ██████  ██ ██   ████
  */

  $sails.on('usrLogin', (usr) => {
    console.log('SOCKET->usrLogin', usr);
    alertify.message('El usuario ' + usr.nombre + ' ha iniciado sesión.');
  });

}]);
