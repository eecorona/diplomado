ng.config(['$httpProvider', '$provide', '$qProvider', '$sailsProvider',
  function($httpProvider, $provide, $qProvider, $sailsProvider) {

    $qProvider.errorOnUnhandledRejections(false);

    $provide.factory('myHttpInterceptor', ['$q', '$rootScope',
      function($q, $rootScope, ) {
        return {
          'request': function(config) {
            if(config.method === 'POST' && config.data){
              //#TODOS: esta lista de campos que nunca se postean aun que se manden, podrian venir
              // de una configuracion de sails que se cargue despues de iniciar sesion por ejemplo.
              var excludedAttrs = ['createdAt', 'updatedAt', 'originalElement', 'esteCampoNuncaSaleEnElPost'];
              _.each(excludedAttrs, (prop) => {
                if (config.data[prop]) {
                  delete config.data[prop];
                }
              });
            }
            if (!config.headers) {config.headers ={};}

            config.headers.apikey='asdasdasd123123123123';
            return config;
          },
          'response': function(response) {
            return response;
          },
          'responseError': function(rejection) {
            console.error('Interceptor Rejection:', rejection.status, rejection);

            if (rejection.status===401) {
              $rootScope.main.usuario.killSession();
            }

            return $q.reject(rejection);
          }
        };
      }]);


    $httpProvider.interceptors.push('myHttpInterceptor');
    $sailsProvider.interceptors.push('myHttpInterceptor');


  }]);
