ng.factory('UsuariosFactory', ['$http', '$location', '$sails', '$timeout',
  function($http, $location, $sails, $timeout){
    return {
      yo:{},
      header:'js/app/templates/header.html',
      usuarios:[],
      /*
    ██       ██████   ██████  ██ ███    ██
    ██      ██    ██ ██       ██ ████   ██
    ██      ██    ██ ██   ███ ██ ██ ██  ██
    ██      ██    ██ ██    ██ ██ ██  ██ ██
    ███████  ██████   ██████  ██ ██   ████
    */

      login: function(usuario, password){
        console.log('Iniciando sesion...');
        var self = this;
        var data = {
          usuario: usuario,
          password: password
        };
        $http.post('/auth/login', data).then(function succcess(response){
          console.log('POST /auth/login->response:', response);
          localStorage.setItem('usuario', JSON.stringify(response.data));
          alertify.success('Bienvenido ' + response.data.nombre);
          self.yo = response.data;
          self.canales();
          //cargamos el header que le corresponde al perfil
          if(self.yo.perfil==='alumno'){
            self.header='js/app/templates/header_alumno.html';
          }

          $location.path('/dashboard').replace();
        },function error(err){
          console.log('POST /auth/login->error:', err);
          if (err.status === 403 && typeof err.data === 'string') {
            alertify.error(err.data);
          }else{
            alertify.error('No se pudo iniciar sesión.');
          }
        });
      },
      /*
    ██       ██████   ██████   ██████  ██    ██ ████████
    ██      ██    ██ ██       ██    ██ ██    ██    ██
    ██      ██    ██ ██   ███ ██    ██ ██    ██    ██
    ██      ██    ██ ██    ██ ██    ██ ██    ██    ██
    ███████  ██████   ██████   ██████   ██████     ██
    */

      logout: function(){
        var self = this;
        alertify.confirm('Salir','¿Está seguro?', () => {
          self.killSession();
        },() => {});
      },
      killSession: function(){
        var self = this;
        $timeout(() => {
          self.header = 'js/app/templates/header.html';//regresarl al default
          self.yo = {};
          localStorage.removeItem('usuario');
          $location.path('/login').replace();
        });
      },
      /*
    ██ ███    ██ ██ ████████
    ██ ████   ██ ██    ██
    ██ ██ ██  ██ ██    ██
    ██ ██  ██ ██ ██    ██
    ██ ██   ████ ██    ██
    */

      init: function(){
        var self = this;
        //ver si ya tenemos un usuario antes de intentar login
        var usr = localStorage.getItem('usuario');
        if(usr) {usr=JSON.parse(usr);}
        if(usr && usr.id){

          self.yo = usr;

          //cargamos el header que le corresponde al perfil
          if(self.yo.perfil==='alumno'){
            self.header='js/app/templates/header_alumno.html';
          }

          //validar remotamente la sesion.
          self.canales();
        }
      },
      canales: function(){
        var self = this;
        //validar remotamente la sesion.
        $sails.get('/usuarios/yo').then(function succcess(response){
          console.log('GET /usuarios/yo->response:', response);
          if(response && response.status!==200){
            self.killSession();
          }
          if (response && response.data && response.data.canales && response.data.canales.length){
            alertify.message('Tus canales ['+response.data.canales.join(', ')+']');
          }
        },function error(err){
          console.log('GET /usuarios/yo->error:', err);
          self.killSession();
        });
      },
      /*
     ██████  ██    ██  █████  ██████  ██████   █████  ██████
    ██       ██    ██ ██   ██ ██   ██ ██   ██ ██   ██ ██   ██
    ██   ███ ██    ██ ███████ ██████  ██   ██ ███████ ██████
    ██    ██ ██    ██ ██   ██ ██   ██ ██   ██ ██   ██ ██   ██
     ██████   ██████  ██   ██ ██   ██ ██████  ██   ██ ██   ██
    */

      guardar: function(data, p, cb){
      //var self = this;
        if(p) {data.password = p;}
        console.log('Usuario a guardar:', data);
        $http.post('/usuarios/guardar', {usuario:data}).then(function succcess(response){
          console.log('POST /usuarios/guardar->response:', response);
          if(typeof cb === 'function') {return cb(null);}
        },function error(err){
          console.log('POST /usuarios/guardar->error:', err);
          alertify.error('Ocurrio un error al guardar');
          if(typeof cb === 'function') {return cb(err);}
        });
      },
      /*
    ██████   ██████  ██████  ██████   █████  ██████
    ██   ██ ██    ██ ██   ██ ██   ██ ██   ██ ██   ██
    ██████  ██    ██ ██████  ██████  ███████ ██████
    ██   ██ ██    ██ ██   ██ ██   ██ ██   ██ ██   ██
    ██████   ██████  ██   ██ ██   ██ ██   ██ ██   ██
    */

      borrar: function(uid, cb){
      //var this = self;
        $http.post('/usuarios/borrar',{id:uid}).then(function succcess(response){
          console.log('POST /usuarios/borrar->response:', response);
          response.data.forEach((borrado) => {
            var idx = _.findIndex(self.usuarios, {id:borrado.id});
            if(idx>-1) {self.usuarios.splice(idx,1);}
          });
          if(typeof cb === 'function') {return cb(null, response);}
        },function error(err){
          console.log('POST /usuarios/borrar->error:', err);
          if(typeof cb === 'function') {return cb(err);}
        });
      },
      /*
     ██████  ███████ ████████  █████  ██      ██
    ██       ██         ██    ██   ██ ██      ██
    ██   ███ █████      ██    ███████ ██      ██
    ██    ██ ██         ██    ██   ██ ██      ██
     ██████  ███████    ██    ██   ██ ███████ ███████
    */

      getAll: function(){
        var self = this;
        $http.get('/usuarios').then(function succcess(response){
          console.log('GET /usuarios->response:', response);
          self.usuarios = response.data;
        },function error(err){
          console.log('GET /usuarios->error:', err);
        });
      },
      /*
       ██████  ██    ██ ██ ████████  █████  ██████
      ██    ██ ██    ██ ██    ██    ██   ██ ██   ██
      ██    ██ ██    ██ ██    ██    ███████ ██████
      ██ ▄▄ ██ ██    ██ ██    ██    ██   ██ ██   ██
       ██████   ██████  ██    ██    ██   ██ ██   ██
          ▀▀
      */

      quitar: function(usuario){
        var self = this;
        var idx = _.findIndex(self.usuarios,{id:usuario.id});
        if(idx > -1){
          self.usuarios.splice(idx, 1);
        }
      }
    };
  }]);
