ng.factory('AlumnosFactory', ['$http',
  function($http){
    return {
      data:[],
      fetch: false,
      /*
     ██████  ██    ██  █████  ██████  ██████   █████  ██████
    ██       ██    ██ ██   ██ ██   ██ ██   ██ ██   ██ ██   ██
    ██   ███ ██    ██ ███████ ██████  ██   ██ ███████ ██████
    ██    ██ ██    ██ ██   ██ ██   ██ ██   ██ ██   ██ ██   ██
     ██████   ██████  ██   ██ ██   ██ ██████  ██   ██ ██   ██
    */

      guardar: function(data, p, cb){
      //var self = this;
        if(p) {data.password = p;}
        console.log('Alumno a guardar:', data);
        $http.post('/alumnos/guardar', {alumno:data}).then(function succcess(response){
          console.log('POST /alumnos/guardar->response:', response);
          if(typeof cb === 'function') {return cb(null);}
        },function error(err){
          console.log('POST /alumnos/guardar->error:', err);
          alertify.error('Ocurrio un error al guardar');
          if(typeof cb === 'function') {return cb(err);}
        });
      },
      /*
    ██████   ██████  ██████  ██████   █████  ██████
    ██   ██ ██    ██ ██   ██ ██   ██ ██   ██ ██   ██
    ██████  ██    ██ ██████  ██████  ███████ ██████
    ██   ██ ██    ██ ██   ██ ██   ██ ██   ██ ██   ██
    ██████   ██████  ██   ██ ██   ██ ██   ██ ██   ██
    */

      borrar: function(uid, cb){
        var self = this;
        $http.post('/alumnos/borrar',{id:uid}).then(function succcess(response){
          console.log('POST /alumnos/borrar->response:', response);
          //extraer de mi panza los borrados
          response.data.forEach((borrado) => {
            var idx = _.findIndex(self.data, {id:borrado.id});
            if(idx>-1) {self.data.splice(idx,1);}
          });
          if(typeof cb === 'function') {return cb(null, response);}
        },function error(err){
          console.log('POST /alumnos/borrar->error:', err);
          if(typeof cb === 'function') {return cb(err);}
        });
      },
      /*
     ██████  ███████ ████████  █████  ██      ██
    ██       ██         ██    ██   ██ ██      ██
    ██   ███ █████      ██    ███████ ██      ██
    ██    ██ ██         ██    ██   ██ ██      ██
     ██████  ███████    ██    ██   ██ ███████ ███████
    */

      getAll: function(){
        var self = this;
        if(!self.fetch){
          $http.get('/alumnos').then(function succcess(response){
            console.log('GET /alumnos->response:', response);
            self.data = response.data;
            self.fetch = true;
          },function error(err){
            console.log('GET /alumnos->error:', err);
          });
        }
      },
      /*
       ██████  ██    ██ ██ ████████  █████  ██████
      ██    ██ ██    ██ ██    ██    ██   ██ ██   ██
      ██    ██ ██    ██ ██    ██    ███████ ██████
      ██ ▄▄ ██ ██    ██ ██    ██    ██   ██ ██   ██
       ██████   ██████  ██    ██    ██   ██ ██   ██
          ▀▀
      */

      quitar: function(alumno){
        var self = this;
        var idx = _.findIndex(self.data,{id:alumno.id});
        if(idx > -1){
          self.data.splice(idx, 1);
        }
      }
    };
  }]);
