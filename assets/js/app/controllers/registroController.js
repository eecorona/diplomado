ng.controller('registroController', ['$rootScope', '$scope', '$http', '$routeParams',
  function($rootScope, $scope, $http, $routeParams) {

    $scope.$on('$viewContentLoaded', function hola() {
      console.log('registroController cargado!');
      $scope.reg = {};
      $scope.ocupado = false;
      $scope.token = '';
      $scope.modo='registro';

      //es una activacion?
      if($routeParams.activar==='activar'){
        $scope.modo='activacion';
        $scope.token = $routeParams.token||'';
        if($scope.token) {$scope.activar();}
      }
    });

    /*
     █████   ██████ ████████ ██ ██    ██  █████  ██████
    ██   ██ ██         ██    ██ ██    ██ ██   ██ ██   ██
    ███████ ██         ██    ██ ██    ██ ███████ ██████
    ██   ██ ██         ██    ██  ██  ██  ██   ██ ██   ██
    ██   ██  ██████    ██    ██   ████   ██   ██ ██   ██
    */

    $scope.activar = function(){
      $scope.ocupado = true;
      var msg = 'No se pudo activar la cuenta.';
      $http.post('/alumnos/activar', {token:$scope.token}).then(function succcess(response){
        console.log('POST /alumnos/activar->response:', response);
        $scope.ocupado = false;
        if(response.data.result){
          $scope.usuarioActivado = response.data.registro;
          alertify.success('Su cuenta ha sido activada, gracias '+$scope.usuarioActivado.nombre);
        }else{
          if(typeof response.data.err === 'string'){
            msg = response.data.err;
          }
          $scope.token='';
          alertify.error(msg);
        }
      },function error(err){
        $scope.ocupado = false;
        console.log('POST /alumnos/activar->error:', err);
        alertify.error(msg);
      });
    };

    /*
    ██████  ███████  ██████  ██ ███████ ████████ ██████   █████  ██████
    ██   ██ ██      ██       ██ ██         ██    ██   ██ ██   ██ ██   ██
    ██████  █████   ██   ███ ██ ███████    ██    ██████  ███████ ██████
    ██   ██ ██      ██    ██ ██      ██    ██    ██   ██ ██   ██ ██   ██
    ██   ██ ███████  ██████  ██ ███████    ██    ██   ██ ██   ██ ██   ██
    */

    $scope.registrar = function(){
      $scope.ocupado = true;
      $http.post('/alumnos/registro',{registro: $scope.reg}).then(
        function success(response){
          $scope.ocupado = false;
          console.log('Resultado de registro:', response);
          if(response.data && response.data.result ){
            alertify.success('Su registro ha sido creado, favor de verificar su email.');
            $scope.reg = {};
          }else{
            alertify.error('No se pudo crear el registro: '+response.data.err);
          }
        },
        function error(err){
          $scope.ocupado = false;
          console.log('Error al registrar alumno:', err);
          alertify.error('No se pudo registrar el alumno.');
        }
      );
    };
  }
]);
