ng.controller('usuarioController', ['$rootScope', '$scope', '$routeParams', '$http', '$location', '$timeout',
  function($rootScope, $scope, $routeParams, $http, $location, $timeout) {
    $scope.$on('$viewContentLoaded', () => {
      console.log('usuarioController loaded!');
      $scope.perfiles = ['admin','supervisor','caja','gerente'];
      $scope.u = {};
      $scope.editando = false;
      if($routeParams.id!=='nuevo'){ //hay que ir por el usuario
        $http.get('/usuarios/'+$routeParams.id).then(function succcess(response){
          console.log('GET /usuarios/->response:', response);
          $scope.u = response.data;
        },function error(err){
          console.log('GET /usuarios/->error:', err);
          alertify.error('Error al obtener el usuario.');
        });
      }
    });

    $scope.$on('usuarioBorrado', (event, data) => {
      if(data.id === $scope.u.id){ //estamos editando el que se borro!
        alertify.alert('Elimnado','El usuario que estaba editando fue borrado', () => {
          $timeout(() => {
            $location.path('/usuarios');
          });
        });
      }
    });

    $scope.editar = function(modo){
      $scope.editando = modo;
      if(modo){
        $scope.respaldoUsuario = angular.copy($scope.u);
      }else{
        $scope.u = $scope.respaldoUsuario;
      }
    };

    $scope.guardarUsuario = function(){
      $rootScope.main.usuario.guardar($scope.u, $scope.password, (err) => {
        if(err) {
          console.log('Error al guardar usuario', err);
          alertify.error('Error al guardar');
        }else{
          $location.path('/usuarios');
        }
      });
    };
  }
]);
