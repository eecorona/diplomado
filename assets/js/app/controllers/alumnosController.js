ng.controller('alumnosController', ['$rootScope', '$scope', '$http', '$timeout',
  function($rootScope, $scope, $http, $timeout) {

    $scope.$on('$viewContentLoaded', function hola() {
      console.log('alumnosController cargado!');
      $rootScope.main.alumnos.getAll();
    });

    $scope.borrar = function(u){
      alertify.confirm('¿Seguro?','Se va a borrar al usuario '+u.nombre,() => {
        $timeout(function hola() {
          $rootScope.main.alumnos.borrar(u.id, (err, response) => {
            if(err){
              console.log('error al borrar', err);
            }
            if(response.status === 200){
              //alumnos borrado satisfactoriamente, refresh a la lista!
              //$rootScope.main.alumnos.getAll();
            }
          });
        });
      }, () => {
        alertify.message('Cancelado');
      });
    };
  }
]);
