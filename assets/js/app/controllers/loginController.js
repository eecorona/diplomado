ng.controller('loginController', ['$rootScope', '$scope',
  function($rootScope, $scope) {

  /**
   * Evento: $scope -> $viewContentLoaded
   *
   * Se dispara cuando una vista se ha finalizado de cargar.
   * Util para inicializar lógica cuando la vista ya fué cargada.
   */
    $scope.$on('$viewContentLoaded', function load() {
      console.log('loginController cargado!');
    });

    /**
   * Defaults
   */
    $scope.usuario = '';
    $scope.password = '';

  }]);
