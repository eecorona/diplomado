ng.controller('usuariosController', ['$rootScope', '$scope', '$http', '$timeout',
  function($rootScope, $scope, $http, $timeout) {

    $scope.$on('$viewContentLoaded', function hola() {
      console.log('usuariosController cargado!');
      $rootScope.main.usuario.getAll();
    });

    $scope.borrar = function(u){
      alertify.confirm('¿Seguro?','Se va a borrar al usuario '+u.nombre,() => {
        $timeout(function hola() {
          $rootScope.main.usuario.borrar(u.id, (err, response) => {
            if(err){
              console.log('error al borrar', err);
            }
            if(response.status === 200){
              //usuario borrado satisfactoriamente, refresh a la lista!
              //$rootScope.main.usuario.getAll();
            }
          });
        });
      }, () => {
        alertify.message('Cancelado');
      });
    };
  }
]);
