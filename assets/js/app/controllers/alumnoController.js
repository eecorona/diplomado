ng.controller('alumnoController', ['$rootScope', '$scope', '$routeParams', '$http', '$location', '$timeout', 'FileUploader',
  function($rootScope, $scope, $routeParams, $http, $location, $timeout, FileUploader) {
    $scope.$on('$viewContentLoaded', () => {
      console.log('alumnoController loaded!');
      $scope.perfiles = ['admin','supervisor','caja','gerente'];
      $scope.a = {
        direccion:{}
      };
      $scope.editando = false;
      if($routeParams.id!=='nuevo'){ //hay que ir por el alumno
        $http.get('/alumnos/'+$routeParams.id).then(function succcess(response){
          console.log('GET /alumnos/->response:', response);
          $scope.a = response.data;
        },function error(err){
          console.log('GET /alumnos/->error:', err);
          alertify.error('Error al obtener el alumno.');
        });
      }

      $scope.tab = 'generales';
    });

    $scope.random = Math.random();

    var uploader = $scope.uploader = new FileUploader({
      url: '/alumnos/guardar-imagen',
      alias: 'imagen',
      autoUpload: true,
      removeAfterUpload: true,
      queueLimit: 1
    });

    $scope.$on('alumnoBorrado', (event, data) => {
      if(data.id === $scope.a.id){ //estamos editando el que se borro!
        alertify.alert('Eliminado','El alumno que estaba editando fue borrado', () => {
          $timeout(() => {
            $location.path('/alumnos');
          });
        });
      }
    });

    $scope.editar = function(modo){
      $scope.editando = modo;
      if(modo){
        $scope.respaldoAlumno = angular.copy($scope.a);
      }else{
        $scope.a = $scope.respaldoAlumno;
      }
    };

    $scope.guardarAlumno = function(){
      $rootScope.main.alumnos.guardar($scope.a, $scope.password, (err) => {
        if(err) {
          console.log('Error al guardar alumno', err);
          alertify.error('Error al guardar');
        }else{
          alertify.message('Alumno guardado');
          $location.path('/alumnos');
        }
      });
    };

    uploader.filters.push({
      name: 'imageFilter',
      fn: function(item) {
        var type = '|' + item.type.slice(item.type.lastIndexOf('/') + 1) + '|';
        return '|jpg|jpeg|'.indexOf(type) !== -1;
      }
    });

    // CALLBACKS

    uploader.onWhenAddingFileFailed = function(item, filter) {
      alertify.error( filter.name==='imageFilter'?'Tipo de archivo no valido':'Sube primero una imagen');
    };

    uploader.onBeforeUploadItem = function(item) {
      console.info('onBeforeUploadItem', item);
      item.formData.push({alumno:$scope.a.id});
    };

    uploader.onSuccessItem = function(fileItem, response, status, headers) {
      console.info('onSuccessItem', fileItem, response, status, headers);
      $timeout(() => {
        $scope.u.foto = true;
        $scope.random = Math.random();
      },100);
    };
  }
]);
