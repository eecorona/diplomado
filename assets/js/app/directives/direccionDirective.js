ng.directive('direccion', ['$http', function($http) {
  return {
    restrict: 'E',
    scope: {
      objeto: '=',
      bloqueado: '=',
      light:'='
    },
    templateUrl: 'js/app/templates/directives/direccionDirective.html',
    link: function(scope){ //el, attrs

      scope.seleccionarCP = function(){
        var idx = _.findIndex(scope.resultadosCP,{asenta:scope.objeto.colonia});
        if(idx>-1){
          var s = scope.resultadosCP[idx];
          scope.objeto.ciudad = s.ciudad;
          scope.objeto.estado = s.estado;
          scope.objeto.colonia = s.asenta;
          scope.objeto.tipoColonia = s.tipoAsenta;
          scope.objeto.municipio = s.mnpio;
        }
      };

      scope.$watch('objeto.cp', (newVal) => {
        if(newVal){
          console.log('El codigo postal ha cambiado:', newVal);
          if(newVal.length===5){
            //consultar el cp en bd y rellenar el objeto de la directiva
            $http.post('/localidades/buscar',{term: scope.objeto.cp}).then(function succcess(response){
              console.log('POST /localidades/buscar->response:', response);
              scope.resultadosCP = response.data;
            },function error(err){
              console.log('POST /localidades/buscar->error:', err);
            });
          }
        }
      });
    },
  };
}]);
