ng.filter('toNow', [function(){
  return function(fecha){
    if(!fecha) {return 'Sin registro';}
    return moment(fecha).fromNow();
  };
}]);
