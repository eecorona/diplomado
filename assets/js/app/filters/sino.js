ng.filter('sino', () => {
  return function(valor, tipo){

    var valorsi='Si';
    var valorno='No';

    if(tipo==='grafico'){
      valorsi='<i class="far fa-check-square" aria-hidden></i>';
      valorno='<i class="far fa-square" aria-hidden></i>';
    }else if(tipo==='manitas'){
      valorsi='<i class="fas fa-thumbs-up" aria-hidden></i>';
      valorno='<i class="fa-thumbs-down" aria-hidden></i>';
    }

    if(valor) {return valorsi;}
    else {return valorno;}
  };
});
